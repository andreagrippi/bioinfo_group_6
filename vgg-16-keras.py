# coding: utf-8

from keras.models import Sequential
from keras.layers.core import Flatten, Dense, Dropout
from keras.layers import Conv2D, MaxPooling2D
from keras.optimizers import SGD
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import cv2, numpy as np
from os import listdir
from os.path import isfile
from PIL import Image
import sys

'''
Dati di input

y[0] = 1 => healthy
y[1] = 1 => non-healthy class
'''

input_x = []
input_y = []
input_val_x = []
input_val_y = []

dir_train = "./crop_train/"
dir_val = "./crop_val/"

file_names = np.array(listdir(dir_train))
np.random.shuffle(file_names)
for f in file_names:
    if isfile(dir_train+f):
        img = np.array(Image.open(dir_train+f))
        img = (img-128)/128
        input_x.append(img)
        splits = f.split("_")
        if splits[1]=='H' :
            input_y.append(0)
        else:
            input_y.append(1)
input_x = np.array(input_x)
input_y = to_categorical(np.array(input_y))
print(input_x.shape)
print(input_y.shape)

file_names = np.array(listdir(dir_val))
for f in file_names:
    if isfile(dir_val+f):
        img = np.array(Image.open(dir_val+f))
        img = (img-128)/128
        input_val_x.append(img)
        splits = f.split("_")
        if splits[1]=='H':
            input_val_y.append(0)
        else:
            input_val_y.append(1)
input_val_x = np.array(input_val_x)
input_val_y = to_categorical(np.array(input_val_y))
'''
Modello
'''

model = Sequential()
model.add(Conv2D(64, (3, 3), activation='relu', padding='same', input_shape=(224,224,3)))
model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
model.add(MaxPooling2D((2,2), strides=(2,2)))

model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
model.add(MaxPooling2D((2,2), strides=(2,2)))

model.add(Conv2D(256, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(256, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(256, (3, 3), activation='relu', padding='same'))
model.add(MaxPooling2D((2,2), strides=(2,2)))

model.add(Conv2D(512, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(512, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(512, (3, 3), activation='relu', padding='same'))
model.add(MaxPooling2D((2,2), strides=(2,2)))

model.add(Conv2D(512, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(512, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(512, (3, 3), activation='relu', padding='same'))
model.add(MaxPooling2D((2,2), strides=(2,2)))

model.add(Flatten())
model.add(Dense(4096, activation='relu'))
model.add(Dense(4096, activation='relu'))
model.add(Dense(2, activation='softmax'))

sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy'])

history = model.fit(x=input_x, y=input_y, epochs=10, batch_size=32, validation_data=(input_val_x, input_val_y))

model.save("vgg-16-model_2_10_sep.h5")

print(history.history.keys())
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
#plt.show()
plt.savefig('vgg_acc_2_10_sep.png')
