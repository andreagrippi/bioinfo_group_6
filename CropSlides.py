# coding: utf-8

# Script for cropping images
#
# * dim_tile = dimension of each square tile
# * overlap_tile = overlap of one tile with the next one
# * level_tile = {0,1} level of detail (see DatasetAnalysis)
#
# Output file name convension: {original file name}_{start_x}_{start_y}.jpg
#

import openslide
from os import listdir
from os.path import isfile, split, join
import cv2
import numpy as np

# ### OpenSlide
#
# #### read_region - Return an RGBA Image containing the contents of the specified region. Unlike in the C interface, the image data is not premultiplied.
# ##### Parameters:
# * location (tuple) – (x, y) tuple giving the top left pixel in the level 0 reference frame
# * level (int) – the level number
# * size (tuple) – (width, height) tuple giving the region size
#
# ##### I valori fanno riferimento all'immagine in level 0.... Facendo delle prove, il fattore di zoom dal livello 0 al livello 1 è 4

def cropImage(src_image_path,dest_folder):
    '''
    Method called by prediction script to crop an image

    :param str path: .svs file location
    :param str dest_folder: folder where crops and the final attention map are saved
    '''
    dim_tile = 224
    overlap_tile = 50
    level_tile = 1

    print("Cutting images...")
    print(src_image_path)
    for f in listdir(src_image_path):
        if isfile(join(src_image_path,f)):
            img = openslide.OpenSlide(join(src_image_path,f))
            dim_x = img.dimensions[0]
            dim_y = img.dimensions[1]
            start_x = 0
            start_y = 0
            while((start_x+dim_tile)*4 <= dim_x):
                while((start_y+dim_tile)*4 <= dim_y):
                    tile = img.read_region((start_x*4,start_y*4),level_tile,(dim_tile,dim_tile))
                    tile = np.array(tile)
                    tile_name = f.split(".")[0]+"_"+str(start_x)+"_"+str(start_y)+".jpg"
                    tile_path = join(dest_folder,tile_name)
                    cv2.imwrite(tile_path,tile)
                    start_y = start_y + dim_tile - overlap_tile
                start_x = start_x + dim_tile - overlap_tile
                start_y = 0
