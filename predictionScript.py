'''
1. crop svs images in tiles
2. predict
3. recombine tiles
'''

from keras.models import load_model
import sys
from os.path import isfile, split, join
from colorCrop import applyOverlay
import numpy as np
from PIL import Image
from CropSlides import cropImage
from CombineSlides import combineImage
from os import listdir
from os.path import isfile
from progressbar import ProgressBar, SimpleProgress

def get_prediction_index(target_class):
    '''
    convertion from class name to the corresponding index in the prediction array
    :param str target_class: class of intrest

    '''
    if target_class == "H":
        return 0
    if target_class == "non-H":
        return 1
    if target_class == "AC":
        return 1
    if target_class == "AD":
        return 2

'''
main
:param str target_class: class of intrest. Choose from: "H","non-H","AC","AD"
:param str model_h5: path to the .h5 file of the trained model to be used for prediction
:param str test_image_path: path to the folder where test images (.svs format) are stored
:param str tile_dest_folder: path to the existing folder where the cropped files will be stored
'''

if len(sys.argv) != 5:
    sys.exit("Usage: python predictionScript.py <target class> <trained CNN model h5 format file> <test image location> <tile destination folder>")

pbarCount = 0
pbar = ProgressBar(widgets=[SimpleProgress()], maxval=10).start()

target_class = sys.argv[1]
print("Loading model: " + sys.argv[2])

pbarCount+=1
pbar.update(pbarCount + 1)

model = load_model(sys.argv[2])
test_image_path = sys.argv[3]
tile_dest_folder = sys.argv[4]

pbarCount+=1
pbar.update(pbarCount + 1)

prediction_index = get_prediction_index(target_class)
print("predict class "+str(prediction_index))
cropImage(test_image_path, tile_dest_folder)
for tile_name in listdir(tile_dest_folder):
    img = np.array(Image.open(join(tile_dest_folder,tile_name)))
    img = (img-128)/128
    img = np.expand_dims(img, axis=0)
    prediction = model.predict(img)
    print(prediction)
    applyOverlay(join(tile_dest_folder,tile_name), prediction[0][prediction_index])
combineImage(tile_dest_folder)

pbar.finish()

print("output file 'output.jpg' has been saved in: "+tile_dest_folder)
