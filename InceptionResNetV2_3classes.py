
# coding: utf-8

# InceptionResNetV2


import tensorflow as tf

from keras.utils import to_categorical
from keras.optimizers import SGD
from keras.models import Model, load_model

import cv2, numpy as np
from os import listdir
from os.path import isfile
from PIL import Image
import matplotlib.pyplot as plt
import sys

# Dati di input
#
# y[0] = 1 => healthy
# y[1] = 1 => AC class
# y[2] = 1 => AD class

input_x = []
input_y = []
input_val_x = []
input_val_y = []

dir_train = "./crop_train/"
dir_val = "./crop_val/"

file_names = np.array(listdir(dir_train))
np.random.shuffle(file_names)
for f in file_names:
    if isfile(dir_train+f):
        img = np.array(Image.open(dir_train+f))
        img = (img-128)/128
        input_x.append(img)
        splits = f.split("_")
        if splits[1]=='H' :
            input_y.append((1,0,0))
        elif splits[1]=='AC' :
            input_y.append((0,1,0))
        elif splits[1]=='AD' :
            input_y.append((0,0,1))
input_x = np.array(input_x)
input_y = np.array(input_y)
print(input_x.shape)
print(input_y.shape)

file_names = np.array(listdir(dir_val))
for f in file_names:
    if isfile(dir_val+f):
        img = np.array(Image.open(dir_val+f))
        img = (img-128)/128
        input_val_x.append(img)
        splits = f.split("_")
        if splits[1]=='H' :
            input_val_y.append((1,0,0))
        elif splits[1]=='AC' :
            input_val_y.append((0,1,0))
        elif splits[1]=='AD' :
            input_val_y.append((0,0,1))
input_val_x = np.array(input_val_x)
input_val_y = np.array(input_val_y)

model = tf.keras.applications.InceptionResNetV2(input_shape=(224,224,3), weights=None, include_top=True, classes=3)

sgd = SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy'])
print(model.output_shape)
model.summary()

history = model.fit(x=input_x, y=input_y, epochs=2, batch_size=32, validation_data=(input_val_x, input_val_y))

model.save_weights('inception_3classes_20_sep.h5')
del model
model = tf.keras.applications.InceptionResNetV2(input_shape=(224,224,3), weights=None, include_top=True, classes=3)
model.load_weights('inception_3classes_20_sep.h5')
print("ok")
print(history.history.keys())
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
#plt.show()
plt.savefig('inception_3classes_20_sep.png')
