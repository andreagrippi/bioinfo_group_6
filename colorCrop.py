from PIL import Image

'''
filename: Path of image to apply overlay to (i.e. "img_folder/7_H_1_0_174.jpg")
prediction: confidence of prediction of cancer on image in floating point
'''

def evaluatePrediction(p):
    if p>=0 and p<40:
        alpha = 255
    if p>=40 and p<70:
        alpha = 200
    if p>=70:
         alpha = 150
    return alpha

def applyOverlay(filename, prediction):
    '''
    Method called to apply overlay to an image according to its prediction value

    :param str path: Picture path
    :param float prediction: Prediction value
    '''

    p = prediction*100
    #Setting level of transparency of overlay
    alpha = evaluatePrediction(p)

    bw = Image.open(filename)

    #create the coloured overlays
    red = Image.new('RGB',bw.size,(255,0,0))

    #create a mask using RGBA to define an alpha channel to make the overlay transparent
    mask = Image.new('RGBA',bw.size,(0,0,0,alpha))

    Image.composite(bw,red,mask).convert('RGB').save(filename)
