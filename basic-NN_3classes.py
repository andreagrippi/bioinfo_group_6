from keras.models import Sequential
from keras.layers.core import Flatten, Dense, Dropout
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.optimizers import SGD
from keras.utils import to_categorical
from keras.layers import Activation, Conv2D

import cv2, numpy as np
from os import listdir
from os.path import isfile
from PIL import Image
import matplotlib.pyplot as plt


'''
Dati di input

y[0] = 1 => healthy
y[1] = 1 => AC class
y[2] = 1 => AD class
'''

input_x = []
input_y = []
input_val_x = []
input_val_y = []

dir_train = "./crop_train/"
dir_val = "./crop_val/"

file_names = np.array(listdir(dir_train))
np.random.shuffle(file_names)
for f in file_names:
    if isfile(dir_train+f):
        img = np.array(Image.open(dir_train+f))
        img = (img-128)/128
        input_x.append(img)
        splits = f.split("_")
        if splits[1]=='H' :
            input_y.append((1,0,0))
        elif splits[1]=='AC' :
            input_y.append((0,1,0))
        elif splits[1]=='AD' :
            input_y.append((0,0,1))
input_x = np.array(input_x)
input_y = np.array(input_y)
print(input_x.shape)
print(input_y.shape)

file_names = np.array(listdir(dir_val))
for f in file_names:
    if isfile(dir_val+f):
        img = np.array(Image.open(dir_val+f))
        img = (img-128)/128
        input_val_x.append(img)
        splits = f.split("_")
        if splits[1]=='H' :
            input_val_y.append((1,0,0))
        elif splits[1]=='AC' :
            input_val_y.append((0,1,0))
        elif splits[1]=='AD' :
            input_val_y.append((0,0,1))
input_val_x = np.array(input_val_x)
input_val_y = np.array(input_val_y)

nClasses = 3

#Modello
model = Sequential()

model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=(224,224,3)))
model.add(Conv2D(32, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(nClasses, activation='softmax'))

model.compile(optimizer='rmsprop', loss='categorical_crossentropy', metrics=['accuracy'])

history = model.fit(x=input_x, y=input_y, epochs=10, batch_size=32, validation_data=(input_val_x, input_val_y))

model.save("basic_3_10_sep.h5")

print(history.history.keys())
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
#plt.show()
plt.savefig('basic_3_10_sep.png')
