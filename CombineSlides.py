import sys
from PIL import Image
import glob
from os import listdir
from os.path import isfile, split, join


def getXY(filename):
    img_name = filename.split(".")
    x, y = img_name[0].split("_")[2], img_name[0].split("_")[3]
    return x, y

def combineImage(crop_folder):
    '''
    Method called by prediction script to recombine the crops of an image

    :param str crop_folder: Path where (crops and) output picture is saved
    '''

    print("Recostructing image...")

    images = []
    X = []
    Y = []

    for filename in listdir(crop_folder):
        file_path = join(crop_folder,filename)
        if isfile(file_path):
            im=Image.open(file_path)
            images.append(im)
            x, y = getXY(filename)
            X.append(int(x))
            Y.append(int(y))
    #print(X)
    total_width =  max(X)+224
    max_height = max(Y)+224

    new_im = Image.new('RGB', (total_width, max_height))

    x_offset = 0
    i = 0

    for im in images:
        new_im.paste(im, (X[i], Y[i]))
        #print(str(X[i]) + " " + str(Y[i]))
        x_offset += im.size[0]
        i += 1

    new_im.save(join(crop_folder,'output.jpg'))
