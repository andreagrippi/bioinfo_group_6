from keras.models import Sequential
from keras.layers.core import Flatten, Dense, Dropout
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
from keras.optimizers import SGD
from keras.utils import to_categorical
from keras.layers import Activation, Conv2D

import cv2, numpy as np
from os import listdir
from os.path import isfile
from PIL import Image
import matplotlib.pyplot as plt


'''
Dati di input

0 = healthy class
1 = non-healthy class
'''

input_x = []
input_y = []

dir = "./crop_cleaned/"

file_names = np.array(listdir(dir))
np.random.shuffle(file_names)
for f in file_names:
    if isfile(dir+f):
        img = np.array(Image.open(dir+f))
        img = (img-128)/128
        input_x.append(img)
        splits = f.split("_")
        if splits[1]=='H' :
            input_y.append(0)
        else:
            input_y.append(1)
input_x = np.array(input_x)
input_y = to_categorical(np.array(input_y))
print(input_x.shape)
print(input_y.shape)

nClasses = 2

#Modello
model = Sequential()

model.add(Conv2D(32, (3, 3), padding='same', activation='relu', input_shape=(224,224,3)))
model.add(Conv2D(32, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
model.add(Conv2D(64, (3, 3), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(nClasses, activation='softmax'))

model.compile(optimizer='rmsprop', loss='binary_crossentropy', metrics=['accuracy'])

history = model.fit(x=input_x, y=input_y, epochs=100, batch_size=32, validation_split=0.3)

model.save("basic_cleaned_100.h5")

print(history.history.keys())
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
#plt.show()
plt.savefig('basic_cleaned_100.png')
